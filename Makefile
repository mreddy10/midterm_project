#  Meena Reddy (mreddy10)                                                                                        
#  Irina Chirca (ichirca1)                                                                                       
#  ichirca1@jhu.edu                                                                                             
#  mreddy10@jhu.edu                                                                                            
#  10/16/17                                                                                                      
#  601.220
#  Midterm Project                                                                                               

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g
# Links together files needed to create executable                              
main: project.o menuUtil.o ppmIO.o imageManip.o
	$(CC) -o main project.o menuUtil.o ppmIO.o imageManip.o -lm -g

project.o: project.c menuUtil.h ppmIO.h imageManip.h 
	$(CC) $(CFLAGS) -c project.c

menuUtil.o: menuUtil.c menuUtil.h
	$(CC) $(CFLAGS) -c menuUtil.c

ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c -lm

clean:
	rm -f *.o main project menuUtil ppmIO imageManip
