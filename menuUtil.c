/* Meena Reddy (mreddy10)                                                                                                                
   Irina Chirca (ichirca1)                                                                                                               
   ichirca1@jhu.edu                                                                                                                      
   mreddy10@jhu.edu                                                                                                                      
   10/16/17                                                                                                                              
   601.220                                                                                                                               
   Midterm Project                                                                                                                       
*/


#include "imageManip.h"

int print_menu(Image *im){
  //Print the menu:
  //printf("r for read\n");
    printf("Main menu:\n r <filename> - read image from <filename>\n w <filename> - write image to <filename>\n s - swap color channels\n br <amt> - change brightness (up or down) by the given amount\n c <x1> <y1><x2> <y2> - crop image to the box with the given corners\n g - convert to grayscale \n cn <amt> - change contrast(up or down) by the given amount\n bl <sigma> - Gaussian blur with the given radius (sigma) \n sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n e <sigma> <threshold> - detect edges with intensity gradient greater than the given threshold\n q - quit\n");

  printf("Enter choice: ");

  char str[1000];
  scanf("%s", str);
  char code = str[0];
  char filename[256];
  double amt; //brightness or contrast amount
  double x1, y1, x2, y2, sigma, sharp;
  x1 = y1 = x2 = y2 = sigma = sharp = 0;

  int file_was_read = 0;

  while (code != 'q'){
    switch (code) {

    case 'r':
      scanf("%s", filename);
      printf("Reading from %s\n", filename);
      read(filename, im);
      file_was_read = 1;
      break;

    case 'w':
	 scanf("%s", filename);
	 write(filename, im);
	 break;

    case 's':
      if (str[1] == 'h'){
	scanf("%lf %lf", &sigma, &amt);
	sharpen(im, sigma, amt); }
      else{
	swap(im); }
      break;
      
    case 'b':
    if (str[1] == 'l'){ 
      if(scanf("%lf", &sigma) == 1){
	blur(im, sigma);}
      else {
	printf("Error: invalid input.\n"); }
    } else {
       scanf("%lf", &amt);
       brightness(im, amt); }
    break;
    
    case 'c':
        if (str[1] == 'n'){
         scanf("%lf", &amt);
	 contrast(im, amt);
       } else {
	  scanf("%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
	  crop(im, x1, y1, x2, y2); }
       break;
       
    case 'g':
      grayscale(im);
      break;
      
    case 'e':
      scanf("%lf %lf", &sigma, &sharp);
      edge_detect(im, sigma, sharp);
      break;
  }
        printf("Main menu:\n r <filename> - read image from <filename>\n w <filename> - write image to <filename>\n s - swap color channels\n br <amt> - change brightness (up or down) by the given amount\n c <x1> <y1><x2> <y2> - crop image to the box with the given corners\n g - convert to grayscale \n cn <amt> - change contrast (up or down) by the given amount\n bl <sigma> - Gaussian blur with the given radius (sigma) \n sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n e <sigma> <threshold> - detect edges with intensity gradient greater than the given threshold\n q - quit\n");
	printf("Enter choice: ");

	str[0] = str[1] = str[2] = '\0';
	scanf("%s", str);
	code = str[0];
  }

  printf("Goodbye!\n");
  if(file_was_read != 0) {free(im->data);}
  free(im);
  return 0;
}

