/* Meena Reddy (mreddy10)                                                                                                                
   Irina Chirca (ichirca1)                                                                                                               
   ichirca1@jhu.edu                                                                                                                      
   mreddy10@jhu.edu                                                                                                                      
   10/16/17                                                                                                                              
   601.220                                                                                                                               
   Midterm Project                                                                                                                       
*/

#ifndef PPMIO_H
#define PPMIO_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

/* A struct to store an RGB pixel, one byte per channel. */
typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

/* A struct to store an image, with pixel data plus useful numbers. */
typedef struct _image {
  Pixel *data;
  int rows;
  int cols;
  int colors;
} Image;

//Function to check if line is comment: 
int isComment(FILE * fp);

//Function to skip comment line:
void parseComment(FILE * fp);

//Function to skip whitespace:
int scanWhitespace(FILE * fp);

//Function to read ppm file:
int read(char *filename, Image *im);

//Function to write a ppm file:
int write(char *filename, Image *im);

//Function to swap color channels:
void swap(Image *im);

//Helper function to clamp:
unsigned char clamp(double new);

//Function to increase brightness:
void brightness(Image *im, double a);

#endif
