/* Meena Reddy (mreddy10)
   Irina Chirca (ichirca1)
   ichirca1@jhu.edu
   mreddy10@jhu.edu
   10/16/17
   601.220
   Midterm Project
*/

/**Main program.*/

#include "ppmIO.h"
#include "menuUtil.h"
#include "imageManip.h"
#define PI 3.14156

int main(){
  //Allocate p space for image struc:
  Image *im = malloc(sizeof(Image));

  //Launch main menu:
  print_menu(im);
  return 0;
}
