/* Meena Reddy (mreddy10)                                                                                                                
   Irina Chirca (ichirca1)                                                                                                               
   ichirca1@jhu.edu                                                                                                                      
   mreddy10@jhu.edu                                                                                                                      
   10/16/17                                                                                                                              
   601.220                                                                                                                               
   Midterm Project                                                                                                                       
*/


#ifndef MENUUTIL_H
#define MENUUTIL_H

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"

//Function to print the menu and process input accordingly. 
int print_menu(Image *im);

#endif
