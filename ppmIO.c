/* Meena Reddy (mreddy10)                                                                                                                
   Irina Chirca (ichirca1)                                                                                                               
   ichirca1@jhu.edu                                                                                                                      
   mreddy10@jhu.edu                                                                                                                      
   10/16/17                                                                                                                              
   601.220                                                                                                                               
   Midterm Project                                                                                                                       
*/


#include "ppmIO.h"

//Function to check if a line in a file is a comment (begins with #):
int isComment(FILE * fp){
  char c; 
  if ((c = fgetc(fp)) == '#'){ 
    return 1; //line is a comment
  } else {
    ungetc(c, fp); //put back the character (move the pointer back a step) 
    return 0;
  }
}

//Function to skip comment line:
void parseComment(FILE * fp){
  char c; 
  while (isComment(fp)){
    while ((c = fgetc(fp)) != '\n');}
}

//Function to skip whitespace between values:
int scanWhitespace(FILE * fp){
  char c = fgetc(fp);  
  if (isspace(c)){
    return 1;
  }
  return 0;
}

//Function to read ppm file:
int read(char *filename, Image *im){
  FILE *fp = fopen(filename, "r");

  //Declare variables for header, rows, columns, colors:
  char header[3];
  int cols;
  int rows;
  int colors;
  
  parseComment(fp);
  fscanf(fp, "%s", header);

  //Check if PPM file:                                               
  if ((strcmp(header, "P6")) != 0){                                             
    printf("Error: invalid file type.");                                        
    return 0;                                                                   
  }                                                                             
  
  scanWhitespace(fp);
  parseComment(fp);

  fscanf(fp, "%d", &cols);

  scanWhitespace(fp);
  parseComment(fp);

  fscanf(fp, "%d", &rows);  

  scanWhitespace(fp);
  parseComment(fp);

  fscanf(fp, "%d", &colors);
  scanWhitespace(fp);
  
  //Check if colors different from 255:
  if (colors != 255){
    printf("Error: invalid color range.");
    return 0;
  }
  
  //Allocate space for pixels
  Pixel *pix = malloc(sizeof(Pixel) * rows * cols);
  
  //Create image struct:
  im->data = pix;
  im->rows = rows;
  im->cols = cols;
  im->colors = colors;
  
  //Read file: 
  int readcount = fread(im->data, sizeof(Pixel), im->rows * cols, fp);
  if (readcount != rows * cols){
    printf("Error: file not read.\n");
  }
  
  //Close file pointer:
  fclose(fp);
  return 0;
}

//Function to write a ppm file:
int write(char *filename, Image *im){
  FILE *fp = fopen(filename, "w");

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}
  
  //Write tag: 
  fprintf(fp, "P6\n");

  //Write dimensions:
  fprintf(fp, "%d %d\n%d\n", im->cols, im->rows, im->colors);

  //Write pixels:
  int written = fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);
  if (written != (im->rows * im->cols)) {
    fprintf(stderr, "Error:ppmIO - failed to write data to file!\n");
  }
  // return written;
  fclose(fp);
  return written;
}


