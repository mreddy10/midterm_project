/* Meena Reddy (mreddy10)                                                                                                                
   Irina Chirca (ichirca1)                                                                                                               
   ichirca1@jhu.edu                                                                                                                      
   mreddy10@jhu.edu                                                                                                                      
   10/16/17                                                                                                                              
   601.220                                                                                                                               
   Midterm Project                                                                                                                       
*/

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include "ppmIO.h"
#include "menuUtil.h"

//Function to swap color channels:                                             
void swap(Image *im);

//Helper function to avoid overflowing pixels (clamp):                          
unsigned char clamp(double new);

//Function to increase brightness:                                              
void brightness(Image *im, double a);

//Function to convert to grayscale:                                             
void grayscale(Image *im);

//Function to crop:
void crop(Image *im, int r_ul, int c_ul, int r_lr, int c_lr);
  
//Helper function to adjust values for contrast:
double adjust(unsigned char val, double amt);

//Function to increase/decrease contrast:
void contrast(Image *im, double amt);

//Function to blur:
void blur(Image *im, double sigma);

//Function to sharpen:
void sharpen(Image *im, double sigma, double sharp);

//Function to detect edges:
void edge_detect (Image *im, double sigma, double threshold);


#endif
