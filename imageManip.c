/* Meena Reddy (mreddy10)                                                     
   Irina Chirca (ichirca1)                                                    
   ichirca1@jhu.edu                                                      
   mreddy10@jhu.edu                                                          
   10/16/17                                                                 
   601.220                                                          
   Midterm Project*/

#include "imageManip.h"
#include "ppmIO.h"
#include <assert.h>

#define PI 3.14159

//Function to swap color channels:
void swap(Image *im){

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}

  int temp;
  for (int i = 0; i < (im->rows * im->cols); i++){
   temp = (((im->data)[i]).r);
   (((im->data)[i]).r) = (((im->data)[i]).g);
   (((im->data)[i]).g) = (((im->data)[i]).b);
   (((im->data)[i]).b) = temp; }
}

//Helper function to avoid overflowing pixels (clamp):
unsigned char clamp(double n){
  if (n >= 0 && n <= 255){
    return n; }
  else if (n > 255){
    return 255; }
  else {
    return 0;}
}

//Function to increase brightness:
void brightness(Image *im, double a){

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}

  for (int i = 0; i < im->rows * im->cols; i++){
    (((im->data)[i]).r) = clamp((((im->data)[i]).r) + a);
    (((im->data)[i]).g) = clamp((((im->data)[i]).g) + a);
    (((im->data)[i]).b) = clamp((((im->data)[i]).b) + a);
  }
}

//Function to convert to grayscale:
void grayscale(Image *im){

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}

  for (int i = 0; i < (im->rows * im->cols); i++){
    int intensity = 0.30*(((im->data)[i]).r) + 0.59*(((im->data)[i]).g) + 0.11*(((im->data)[i]).b);
    (((im->data)[i]).r) = intensity;
    (((im->data)[i]).g) = intensity;
    (((im->data)[i]).b) = intensity;
  }
}

//Function to crop:
void crop(Image *im, int r_ul, int c_ul, int r_lr, int c_lr){ //r_ul = row value upper-left corner; c_ul = column value lower-right corner;

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}

  //Calculate number of columns and rows:
  int c_rows = c_lr - c_ul + 1;                                              
  int c_cols = r_lr - r_ul + 1; 

  //Allocate memory for cropped image:
  Pixel *cropped_pix = malloc(sizeof(Pixel) * c_rows * c_cols);
  Image *cropped_im = malloc(sizeof(Image));
  cropped_im->data = cropped_pix;
  cropped_im->rows = c_rows;
  cropped_im->cols = c_cols;
  cropped_im->colors = im->colors;

  int x; //buffer for row index
  int y; //buffer for column index
  int c = 0; //buffer for new pix array
  
  //Copy cropped matrix into new image:
  for (x = 0; x < c_rows;x++){
    for (y = 0; y < c_cols; y++){
      cropped_im->data[c] = (im->data)[((r_ul + x)*im->cols)+(c_ul + y)];
      c++;
    }
  }
  
  free(im->data);
  im->data = cropped_im->data;
  im->rows = cropped_im->rows;
  im->cols = cropped_im->cols;
  im->colors = cropped_im->colors;
  free(cropped_im); 
}


//Helper function to adjust pixel values for contrast:
double adjust(unsigned char val, double amt){
  double new_val;
  new_val = (double)val;
  double adjusted = (((((new_val/255.0) - 0.5) * amt) + 0.5) *  255.0);
  return adjusted;
}

//Function to increase/decrease contrast:
void contrast(Image *im, double amt){
  for (int i = 0; i < (im->rows * im->cols); i++){
    double new_red = adjust((((im->data)[i]).r), amt);
    double new_green = adjust((((im->data)[i]).g), amt);
    double new_blue = adjust((((im->data)[i]).b), amt);
    (((im->data)[i]).r) = clamp(new_red);
    (((im->data)[i]).g) = clamp(new_green);
    (((im->data)[i]).b) = clamp(new_blue);
  }
 }

//Helper function to square a double:
double sq(double a){
  double n = a * a;
  return n;
}

//Function to blur image:
void blur(Image *im, double sigma){

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}

  //Create new image that will be blurred:
  int n = (int)(sigma * 10); //dimensions of matrix:
  if ((n % 2) ==  0){
    n = n + 1;}

  //Create matrix array:                                                         
  double gaussian[n][n];                                                                                                                        
  //Pixel blur_pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  Image *blur_im = malloc(sizeof(Image));
  Pixel *blur_pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  blur_im->data = blur_pix;
  blur_im->rows = im->rows;
  blur_im->cols = im->cols;
  blur_im->colors = im->colors;

  int center = (n / 2);

  //Set gaussian function:
  int dx; //distance from center column
  int dy; //distance from center rows
  double g; //gaussian value                                                                                                                                                                           
  for (int r = 0; r < n; r++){
    for (int c = 0; c < n; c++){
      dx = abs(c - center);
      dy = abs(r - center);
      g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
      gaussian[r][c] = g;                                            
    }
  }

  //Loop through image:
  for (int r = 0; r < im->rows; r++){
    for (int c = 0; c < im->cols; c++){
     int loc = ((r * im->cols) + c);
     double rsum = 0;
     double gsum = 0;
     double bsum = 0;
     double totalsum = 0;
     
     for (int i = 0; i < n; i++){
       for (int j = 0; j < n; j++){
	 int rloc = ((r - (n/2)) + i);
	 int cloc = ((c - (n/2)) + j);

	 if ((rloc >=0 && rloc<(im->rows)) && (cloc >=0 && cloc<(im->cols))){
	   int pixloc = ((rloc * im->cols) + cloc);
	   rsum += ((((im->data)[pixloc]).r) * gaussian[i][j]);
	   gsum += ((((im->data)[pixloc]).g) * gaussian[i][j]);
	   bsum += ((((im->data)[pixloc]).b) * gaussian[i][j]);
	   totalsum += gaussian[i][j];
	 }
       }
     }
     (((blur_im->data)[loc]).r) = (unsigned char)clamp(rsum / totalsum);       
     (((blur_im->data)[loc]).g) = (unsigned char)clamp(gsum / totalsum);
     (((blur_im->data)[loc]).b) = (unsigned char)clamp(bsum / totalsum);
    }
  }
  free(im->data); 
  im->data = blur_im->data;
  free(blur_im);  
}

//Function to sharpen image:
void sharpen(Image *im, double sigma, double sharp){

  //Make copy:
  Pixel *copy_pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  Image *copy_im = malloc(sizeof(Image));
  
  for (int i = 0; i < im->rows * im->cols; i++){
    copy_pix[i] = im->data[i]; }

  copy_im->data = copy_pix;
  copy_im->rows = im->rows;
  copy_im->cols = im->cols;
  copy_im->colors = im->colors;

  //Create function with Gaussian blur:
  blur(copy_im, sigma);

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}
    
  //Loop over pixels and compare image:
  for (int r = 0; r < im->rows; r++){
    for (int c = 0; c < im->cols; c++){

      //Calculate difference: 
      int loc = (r * im->cols) + c;
      signed char rdif = (((im->data)[loc]).r) - (((copy_im->data)[loc]).r);
      signed char gdif = (((im->data)[loc]).g) - (((copy_im->data)[loc]).g);
      signed char bdif = (((im->data)[loc]).b) - (((copy_im->data)[loc]).b);

      double rscale = rdif * sharp;
      double gscale = gdif * sharp;
      double bscale = bdif * sharp;

      (((im->data)[loc]).r) = clamp((((im->data)[loc]).r) + rscale);
      (((im->data)[loc]).g) = clamp((((im->data)[loc]).g) + gscale);
      (((im->data)[loc]).b) = clamp((((im->data)[loc]).b) + bscale);
    }
  }
  free(copy_im->data);
  free(copy_im);
}

//Function to detect edges:
void edge_detect (Image *im, double sigma, double threshold){
  
  grayscale(im);
  blur(im, sigma);

  if(im->data == NULL){
    printf("Error. Please read a file.\n");}
    
  Pixel *copy_pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  Image *copy_im = malloc(sizeof(Image));
  copy_im->data = copy_pix;
  copy_im->rows = im->rows;
  copy_im->cols = im->cols;
  copy_im->colors = im->colors;
  
  //Loop through image, excluding boundaries:          
  for (int r = 1; r < (im->rows -1); r++){
    for (int c = 1; c < (im->cols - 1); c++){
      int pixloc = (r * im->cols) + c; //pixel location from x and y coordinates                                 
      int pix_up = pixloc + im->cols;
      int pix_down = pixloc - im->cols;
      int pix_left = pixloc - 1;
      int pix_right = pixloc + 1 ;
      
      double  h_gradient = 1.0 * (im->data[pix_right].r - im->data[pix_left].r)/2;
      double  v_gradient = 1.0 * ((((im->data)[pix_down]).r) - (((im->data)[pix_up]).r))/2;
      double  gradient_magnitude = sqrt(sq(h_gradient) + sq(v_gradient));
    
       if(gradient_magnitude < threshold){ 
        (((copy_im->data)[pixloc]).r) = 255;
        (((copy_im->data)[pixloc]).g) = 255;
        (((copy_im->data)[pixloc]).b) = 255;}

        if(gradient_magnitude > threshold){
	(((copy_im->data)[pixloc]).r) = 0;
        (((copy_im->data)[pixloc]).g) = 0;
        (((copy_im->data)[pixloc]).b) = 0;}
    }
  }
  free(im->data);
  im->data = copy_im->data;
  free(copy_im); 
}
